# Payment Api Developed with Serverless Framework

## URL

`https://2gfe7g0nbe.execute-api.us-east-1.amazonaws.com/api/payments`

### Programming Language

`Python (version 3.8)`

### Prerequisites

In order to package your dependencies locally with `serverless-python-requirements`, you need to have `Python3.8` installed locally. You can create and activate a dedicated virtual environment with the following command:

```bash
python3.8 -m venv ./venv
source ./venv/bin/activate
```

Also `Node js`, preferred version `>= 16.0 `


### Deployment

install dependencies with:

```
npm install
```

and

```
pip install -r requirements.txt
```

and then perform deployment with:

```
serverless deploy
```

After running deploy, you should see output similar to:

```bash
Deploying salon-notification-service-api to stage dev (us-east-1)

✔ Service deployed to stack salon-payment-mngt-api (182s)

endpoint: ANY - https://2gfe7g0nbe.execute-api.us-east-1.amazonaws.com
functions:
  api: salon-payment-mngt-api-dev-api (1.5 MB)
```

_Note_: In current form, after deployment, your API is public and can be invoked by anyone. For production deployments, you might want to configure an authorizer. For details on how to do that, refer to [httpApi event docs](https://www.serverless.com/framework/docs/providers/aws/events/http-api/).

### Invocation

After successful deployment, you can call the created application via HTTP:

```bash
curl https://2gfe7g0nbe.execute-api.us-east-1.amazonaws.com/api
```

Which should result in the following response:

```
{"message":"route not found"}
```

Make a payment :
### request
```bash
(POST) https://2gfe7g0nbe.execute-api.us-east-1.amazonaws.com/api/payments
{
    "card_number": "4242424242424242",
    "exp_month": 11,
    "exp_year": 25,
    "cvc": 678,
    "price": 16,
    "service_name": "Hairs Cuts"
}
```

Should result in the following response:

```bash
{
  "receipt_url":"some_url_for_the_receipt"
}
```

Get all the payments:
### request
```bash
(GET) https://2gfe7g0nbe.execute-api.us-east-1.amazonaws.com/api/payments

```
Should result a list payments:

```bash
[
    {
        "failure_code": null,
        "is_refunded": false,
        "payment_date": "2022-04-16 14:13:40",
        "status": "succeeded",
        "amount": "60.0",
        "receipt_url": "https://pay.stripe.com/receipts/acct_1KojfOKcWkZWjMCd/ch_3KpCNbKcWkZWjMCd0yviHduj/rcpt_LWErhC1zI3wx51uIUjdzIKBqlpHSSJ7",
        "failure_message": null,
        "id": "ch_3KpCNbKcWkZWjMCd0yviHduj",
        "paid": true
    }
]
```

Get the details of a payment:
### request
```bash
(GET) https://2gfe7g0nbe.execute-api.us-east-1.amazonaws.com/api/payments/{payment_id}

```
Should result a single payment or 404:

```bash
 {
        "failure_code": null,
        "is_refunded": false,
        "payment_date": "2022-04-16 14:13:40",
        "status": "succeeded",
        "amount": "60.0",
        "receipt_url": "https://pay.stripe.com/receipts/acct_1KojfOKcWkZWjMCd/ch_3KpCNbKcWkZWjMCd0yviHduj/rcpt_LWErhC1zI3wx51uIUjdzIKBqlpHSSJ7",
        "failure_message": null,
        "id": "ch_3KpCNbKcWkZWjMCd0yviHduj",
        "paid": true
    },
```
### Local development

Thanks to capabilities of `serverless-wsgi`, it is also possible to run your application locally, however, in order to do that, you will need to first install `werkzeug` dependency, as well as all other dependencies listed in `requirements.txt`. It is recommended to use a dedicated virtual environment for that purpose. You can install all needed dependencies with the following commands:

```bash
pip install werkzeug
pip install -r requirements.txt
```

At this point, you can run your application locally with the following command:

```bash
serverless wsgi serve
```
